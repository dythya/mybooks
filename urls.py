from django.urls import path

from mybooks import views
from mybooks.views import (AuthorList, AuthorListRecent, BookView, BooksAlpha,
                           BooksOrderRead,
                           BooksToRead, SeriesList)

app_name = 'mybooks'
urlpatterns = [
    # Index Views

    # / Latest 10 books read
    path('', BookView.as_view(), name='books'),
    # Book /books  # order alphabetical
    path('books/', BooksAlpha.as_view(), name='allbooks_alphabetical'),
    # Book /books_orderread  # order read
    path('books_orderread/', BooksOrderRead.as_view(),
         name='allbooks_orderread'),
    # Book /read  - books to read next
    path('read/', BooksToRead.as_view(), name='read'),
    path('read/soon/', BooksToRead.as_view(), name='now'),
    path('read/now/', BooksToRead.as_view(), name='soon'),

    # Author /author
    path('authors/', AuthorList.as_view(), name='authors'),
    path('authors_recent/', AuthorListRecent.as_view(), name='authors_recent'),

    # Series /series
    path('series/', SeriesList.as_view(), name='series'),

    # Detail Views all /model/pk e.g. /mybooks/book/5
    path('book/<int:pk>/', views.BookDetail.as_view(),
         name='book_detail'),
    path('author/<int:pk>/', views.AuthorDetail.as_view(),
         name='author_detail'),
    path('series/<int:pk>/', views.SeriesDetail.as_view(),
         name='series_detail'),

    # Series given book id /mybooks/5/series not sure why take this route
    path('<int:pk>/series/', views.BookSeries.as_view()),
    path('book/<int:pk>/series/', views.BookSeries.as_view(),
         name='book_series'),

    # Author given book id /mybooks/5/author not sure why take this route
    path('book/<int:pk>/author/', views.BookAuthor.as_view(),
         name='book_author'),
    path('<int:pk>/author/', views.BookAuthor.as_view()),

    # Views for editing
    # model/id/update/
    path('author/<int:pk>/update/', views.AuthorUpdate.as_view(),
         name='author_update'),
]
