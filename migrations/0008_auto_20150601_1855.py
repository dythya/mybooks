# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0007_auto_20150530_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='status',
            field=models.CharField(choices=[('R', 'Read'), ('RS', 'Read Soon'), ('M', 'Maybe Read'), ('N', 'Read Next'), ('S', 'Somewhere'), ('B', 'Blocked in a series')], max_length=2, default='M'),
        ),
    ]
