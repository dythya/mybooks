# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='author',
            options={'ordering': ['last', 'first']},
        ),
        migrations.AlterField(
            model_name='author',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='book',
            name='status',
            field=models.CharField(default='M', choices=[('R', 'Read'), ('S', 'Read Soon'), ('M', 'Maybe Read'), ('N', 'Read Next'), ('M', 'Somewhere')], max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='series',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='series',
            name='status',
            field=models.CharField(default='L', choices=[('S', 'Started Reading'), ('F', 'Series Complete'), ('W', 'Waiting next book'), ('U', 'Not even read book 1'), ('L', 'Not added a status')], max_length=2),
            preserve_default=True,
        ),
    ]
