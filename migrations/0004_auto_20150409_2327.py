# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0003_auto_20150405_1715'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='author',
            name='date_added',
        ),
        migrations.RemoveField(
            model_name='book',
            name='date_added',
        ),
        migrations.RemoveField(
            model_name='series',
            name='date_added',
        ),
    ]
