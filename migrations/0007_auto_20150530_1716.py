# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0006_auto_20150523_1620'),
    ]

    operations = [
        migrations.AddField(
            model_name='series',
            name='primary_author',
            field=models.ForeignKey(null=True, to='mybooks.Author', blank=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='author',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='status',
            field=models.CharField(choices=[('R', 'Read'), ('RS', 'Read Soon'), ('M', 'Maybe Read'), ('N', 'Read Next'), ('S', 'Somewhere')], max_length=2, default='M'),
        ),
        migrations.AlterField(
            model_name='series',
            name='date_added',
            field=models.DateTimeField(verbose_name='Date added', auto_now_add=True),
        ),
    ]
