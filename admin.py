from django.contrib import admin

from .models import Author, Series, Book


class AuthorsInline(admin.TabularInline):
    model = Author
    extra = 0
    fieldsets = [(
        None,
        {'fields': ['first', 'last', 'rating']})]


class BooksInline(admin.TabularInline):
    model = Book
    extra = 1
    fieldsets = [(
        None,
        {'fields': ['title', 'author', 'series', 'series_book_number',
         'rating', 'status', 'date_read', 'local_library']})]


class SeriesInline(admin.TabularInline):
    model = Series
    extra = 1
    fieldsets = [(
        None,
        {'fields': ['name', 'primary_author', 'status', 'series_number',
         'rating']})]


class BookAdmin(admin.ModelAdmin):
    list_display = ('author', 'title', 'series', 'series_book_number',
                    'status', 'local_library', 'date_read', 'date_changed')
    # tried to add read_recently but it didn't like it
    list_filter = ['status', 'date_added', 'author', 'local_library']
    search_fields = ['title', 'author__last', 'author__first', 'series__name']
    readonly_fields = ('date_added', 'date_changed')
    fieldsets = [
        (None, {'fields': ['title', 'author', 'series', 'series_book_number',
                           'status', 'date_read', 'rating', 'local_library',
                           'date_changed', 'date_added', ]}),
        ('Notes', {'fields': ['description', 'notes'],
                   'classes': ['collapse']}),
        ]


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('last', 'first', 'rating', 'date_added')
    search_fields = ['last', 'first']
    list_filter = ['date_added']
    readonly_fields = ('date_added',)
    fieldsets = [
        (None, {'fields': ['first', 'last', 'date_added']}),
        ('Notes', {'fields': ['notes', 'rating'], 'classes': ['collapse']})
    ]
    inlines = [SeriesInline, BooksInline]


class SeriesAdmin(admin.ModelAdmin):
    list_display = ('name', 'next_book', 'status', 'date_changed', 'date_added')
    search_fields = ['name']
    readonly_fields = ('date_added', 'date_changed',)
    list_filter = ['status', 'date_added']
    inlines = [BooksInline]

# Register your models here.
admin.site.register(Author, AuthorAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Book, BookAdmin)
