import datetime

from django.db import models


class Author(models.Model):
    '''Book Authors'''
    first = models.CharField(max_length=25)
    last = models.CharField(max_length=25)
    notes = models.TextField(null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True)
    date_added = models.DateTimeField("Date added", auto_now_add=True)

    def __str__(self):
        return "{0} {1}".format(self.last, self.first)

    def author(self):
        return "{0} {1}".format(self.first, self.last)

    def books_read(self):
        return Book.objects.filter(author=self, status=Book.READ).count() > 0

    def number_books_read(self):
        return Book.objects.filter(author=self, status=Book.READ).count()

    class Meta:
        ordering = ('last', 'first',)


class Series(models.Model):
    '''Series of books'''
    STARTED = 'S'
    FINISHED = 'F'
    WAITING = 'W'
    UNSTARTED = 'U'
    LAZY = 'L'
    MAYBE_FINISH = 'M'

    STATUS_CHOICES = (
        (STARTED, 'Started Reading'),
        (FINISHED, 'Series Complete'),
        (WAITING, 'Waiting next book'),
        (UNSTARTED, 'Not even read book 1'),
        (LAZY, 'Not added a status'),
        (MAYBE_FINISH, 'Started, maybe finish'))

    STATUS_DISPLAY = {
        STARTED: 'Started',
        FINISHED: 'Complete',
        WAITING: 'Waiting next book',
        UNSTARTED: 'Not even read book 1',
        LAZY: 'Not added a status',
        MAYBE_FINISH: 'maybe finish'}

    name = models.CharField(max_length=50)
    primary_author = models.ForeignKey(Author, null=True, blank=True,
                                       on_delete=models.SET_NULL)
    # wanted to this a foriengn key but can't go in both directions
    # title of next book in series
    next_book = models.CharField(null=True, blank=True, max_length=100)
    status = models.CharField(max_length=2,
                              choices=STATUS_CHOICES,
                              default=LAZY)
    rating = models.IntegerField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    # series can prequel and be in an order
    series_number = models.CharField(null=True, blank=True, max_length=50)
    next = models.ForeignKey("self", null=True, blank=True,
                             on_delete=models.SET_NULL)
    date_added = models.DateTimeField("Date added", auto_now_add=True)
    date_changed = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0}".format(self.name)

    def next_in_series(self):
        return "{0}".format(self.next_book)

    def display_status(self):
        return Series.STATUS_DISPLAY[self.status]

    def unstarted(self):
        return self.status == Series.UNSTARTED and self.status != Series.LAZY

    class Meta:
        ordering = ('primary_author', 'name',)
        verbose_name_plural = "series"


class Book(models.Model):
    READ = 'R'
    SOON = 'RS'
    MAYBE = 'M'
    NEXT = 'N'
    MIDDLE = 'S'
    BLOCK = 'B'
    CHECK = 'C'
    STATUS_DISPLAY = {
        READ: 'Read',
        SOON: 'Read Soon',
        MAYBE: 'Maybe Read',
        NEXT: 'Read Next',
        MIDDLE: 'Reading',
        BLOCK: 'Blocked in a series',
        CHECK: 'Read?'
    }
    STATUS_CHOICES = (
        (READ, STATUS_DISPLAY[READ]),
        (SOON, STATUS_DISPLAY[SOON]),
        (MAYBE, STATUS_DISPLAY[MAYBE]),
        (NEXT, STATUS_DISPLAY[NEXT]),
        (MIDDLE, STATUS_DISPLAY[MIDDLE]),
        (BLOCK, STATUS_DISPLAY[BLOCK]),
        (CHECK, STATUS_DISPLAY[CHECK]),
    )
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, null=True,
                               on_delete=models.SET_NULL)
    series = models.ForeignKey(Series, db_index=False, null=True, blank=True,
                               on_delete=models.SET_NULL)
    # where in series
    series_book_number = models.IntegerField(null=True, blank=True)
    status = models.CharField(max_length=2,
                              choices=STATUS_CHOICES,
                              default=MAYBE)
    description = models.TextField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True)
    date_read = models.DateField(null=True, blank=True)
    date_added = models.DateTimeField("Date added", auto_now_add=True)
    date_changed = models.DateTimeField(auto_now=True)
    local_library = models.NullBooleanField(null=True, blank=True)

    def __str__(self):
        return "{0}".format(self.title)

    def print_status(self):
        return Book.STATUS_DISPLAY[self.status]

    def read_recently(self):
        if not self.date_read:
            return '-'
        days = datetime.date.today() - self.date_read
        if days > datetime.timedelta(180):
            return self.date_read.strftime('%Y')
        return self.date_read.strftime('%-d %b')

    read_recently.admin_order_field = 'date_read'
    read_recently.short_description = 'recent'

    def last_changed(self):
        return self.date_changed.strftime('%d-%m-%Y')

    def short_description(self):
        if self.description:
            return self.description[:25]
        else:
            return ''

    def next_book(self):
        if self.series:
            if self.series_next:
                return self.str
            if self.series_book_number:
                return "Unknown but this was book {0}".\
                        format(self.series_book_number)
            return "Unknown"
        else:
            return "None"

    class Meta:
        ordering = ('title',)
