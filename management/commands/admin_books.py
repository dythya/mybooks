#!/usr/bin/env python3

# A collection of useful functions

from django.core.management.base import BaseCommand
from mybooks.models import Book, Series, Author
import logging
import logging.config

logging.config.fileConfig('ryba/logging_dev.conf')  # Make this a variable next
logger = logging.getLogger('commandLine')

# These should all be placed in config
DEFAULT_DATE_READ = '2001-01-01'


class Command(BaseCommand):
    help = "Housekeeping on series"

    def add_arguments(self, parser):
        parser.add_argument(
            'type', nargs='+', type=str,
            help='Item to house keep books, series or author')

    def handle(self, *args, **options):
        logger.info("Housekeeping {}".format(options['type']))
        for type in options['type']:
            if type == 'series':
                self._housekeep_series()
            elif type == 'book':
                self._housekeep_books()
            elif type == 'author':
                self._housekeep_authors()
            else:
                print("You have entered an invalid type: {}.\n"
                      "Valid types are author, book, series.".format(type))

    def _housekeep_authors(self):
        for a in Author.objects.all():
            print(a)

    def _housekeep_books(self):
        """Read book but no read date just add a default read date"""
        for b in Book.objects.all():
            if b.status == Book.READ and b.date_read is None:
                logger.info('Updating book {}. Read but no date'.format(b))
                b.date_read = DEFAULT_DATE_READ
                b.save()

    def _housekeep_series(self):
        """
        Got a series that has at least one book update author and status if missing

        Books in series are order by series_book_number it just takes the first
        """
        for s in Series.objects.all():
            logger.debug("Series {}".format(s))
            books = Book.objects.order_by('series_book_number').filter(series=s)
            logger.debug("Series books {}".format(books))
            first = True
            for b in books:
                if first:
                    first = False
                    if s.primary_author is None:
                        s.primary_author = b.author
                        s.save()
                        logger.info(
                            'Updating series {} author to be {}'.
                            format(s, b.author))
                    if s.status is None or s.status == Series.LAZY:
                        if b.status == Book.READ:
                            utype = 'started'
                            s.status = Series.STARTED
                        else:
                            utype = 'Not Started'
                            s.status = Series.UNSTARTED
                        s.save()
                        logger.info(
                            "Updating status of {} to {}".format(s, utype))
