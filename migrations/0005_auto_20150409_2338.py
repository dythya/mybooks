# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0004_auto_20150409_2327'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='date_added',
            field=models.DateTimeField(default='2014-11-05 11:11:11', verbose_name=b'Date added', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='date_added',
            field=models.DateTimeField(default='2014-11-05 11:11:11', verbose_name=b'Date added', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='series',
            name='date_added',
            field=models.DateTimeField(default='2014-11-05 11:11:11', verbose_name=b'Date added', auto_now_add=True),
            preserve_default=False,
        ),
    ]
