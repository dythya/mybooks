# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0005_auto_20150409_2338'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='series',
            options={'ordering': ['name'], 'verbose_name_plural': 'series'},
        ),
        migrations.AlterField(
            model_name='book',
            name='rating',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='status',
            field=models.CharField(default=b'M', max_length=2, choices=[(b'R', b'Read'), (b'RS', b'Read Soon'), (b'M', b'Maybe Read'), (b'N', b'Read Next'), (b'S', b'Somewhere')]),
        ),
    ]
