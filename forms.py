from django import forms


class AuthorForm(forms.Form):
    '''Parcel Form option used is used by by Hubbed services

     They hubbed services expect to talk to LabelAPI V1 so send
     a response to a form.
     '''
    first = forms.CharField(label='First', max_length=25)
    last = forms.CharField(label='Last', max_length=25)
    notes = forms.CharField(label='Notes', required=False, max_length=256)
    rating = forms.IntegerField(label='rating', required=False)
