#!/usr/bin/env python3

import re
import csv
from datetime import datetime
from django.core.management.base import BaseCommand
from mybooks.models import Book, Series, Author
import logging
import logging.config

logging.config.fileConfig('ryba/logging_dev.conf')  # Make this a variable next
logger = logging.getLogger('commandLine')


class Command(BaseCommand):
    help = "Import csv feed from GoodReads export file"
    generic_description = 'Imported from Goodreads.'
    goodreads_mybooks_status = {
        'read': Book.READ,
        'to-read': Book.SOON,
        'currently-reading': Book.MIDDLE
        }

    def add_arguments(self, parser):
        parser.add_argument('file', nargs='+', type=str,
                            help='GoodReads CSV file books to import')

    def handle(self, *args, **options):
        for file in options['file']:
            logger.info("Importing books from {}".format(file))
            try:
                with open(file) as f:
                    reader = csv.DictReader(f)
                    self._import_books(reader)
            except FileNotFoundError:
                logger.warning("ERROR: File could not be found {}\n\n".
                               format(file))
                continue

    def _import_books(self, reader):
        for gr_book in reader:
            if gr_book['Exclusive Shelf'] in self.goodreads_mybooks_status.keys():
                book_status = self.goodreads_mybooks_status[
                    gr_book['Exclusive Shelf']]
            else:
                # Not interested in books with this status so get the next one
                msg = "Skipping status {0}: {1} by {2}"\
                    .format(gr_book['Exclusive Shelf'],
                            gr_book['Title'], gr_book['Author'])
                logger.info(msg)
                continue

            date_read = self._get_date_read(book_status, gr_book['Date Read'])

            author = self._get_author(gr_book['Author l-f'])
            title, series, series_number, notes = self._get_title_and_series(
                gr_book['Title'], author)
            self._book_create_or_maybe_update(gr_book, title, author, series,
                                              series_number, notes, date_read,
                                              book_status)

    def _get_author(self, author):
        """
        Get or create author. returns object
        input author:
            Corey, James S.A.
        """
        author_names = author.split(', ')
        author, created = Author.objects.get_or_create(first=author_names[1],
                                                       last=author_names[0])
        if created:
            logger.info("\tCreated Author: {}" .format(author))
        return author

    def _get_date_read(self, book_status, read):
        """
        Get the date read if the book has been read.
        Args:
          status - mybooks book status
          good reads date e.g. 2015/12/28
        Returns:
          None if the book has not been read
        """
        re_date = re.compile('^(\d+)[/-](\d+)[/-](\d+)')
        # split date so can add to date time object
        if book_status == Book.READ and read:
            date_read_parts = re_date.match(read)
            date_read = datetime(int(date_read_parts.group(1)),
                                 int(date_read_parts.group(2)),
                                 int(date_read_parts.group(3)))
        else:
            date_read = None
        return date_read

    def _get_title_and_series(self, gr_title, author):
        """
        Get the series object and title

        If this Good Reads book is a series try to find and create if not found

        Always assume book is part of a series if the title contains ()
        If the book is a series the series name is part of the title.
           The Rogue (Traitor Spy Trilogy, #2)

        The primary author is set to the author found in this gr record

        Args:
           title, author
        Returns:
           book title - with any series info stripped
           series title - None if not a series
           series number - None if not a series
           notes - used for book - may contain series information. For instance
                   if a book is in two series or series name was got by match.
        """
        # seperate book and series titles see above
        # need to look for () at end of the title, if found assume series
        notes = None
        series_title_parts = re.search(r'(.+)\(([^(]+)\)$', gr_title)
        if series_title_parts:
            book_title = series_title_parts[1]
            # remove the trailing )
            series_title_components = series_title_parts[2]
            series_title, book_number, notes = self._get_series_name_number(
                series_title_components)
            series = self._find_series(series_title)
            if not series:
                series = Series.objects.\
                    create(name=series_title,
                           description=self.generic_description,
                           series_number='', primary_author=author)
                logger.info("\tCreated Series: {}".format(series_title))
        else:
            # not a series
            book_title = gr_title
            series = None
            book_number = None
        # book name to long so truncate here otherwise end up with
        # book added multiple times
        if len(book_title) > Book._meta.get_field('title').max_length:
            notes = ' '.join(filter(None, (notes, 'Full title:', book_title)))
            book_title = book_title[0:Book._meta.get_field('title').max_length]
        return book_title, series, book_number, notes

    def _get_series_name_number(self, series_title_components):
        """
        Split up series title into its component parts

        A series name consists of name, number and maybe more series info
        e.g.
           The Rogue (Traitor Spy Trilogy, #2)
        Sometimes things are a member of two series e.g.
           Equal Rites (Discworld, #3; Witches #1)
        In this case the second series is placed in the notes.
        Maybe in future could allow a book to be in two series - kind of weird.

        Args: series_title_components

        Return:
          series_title - title of the series -
          series_book_number - number of the book within the series
                             - 0 if there is really numbering
          notes - extra info like stuff following ;, which is places in series
                  notes
                - in some cases the stuff in () is more like a comment so put
                  in notes.
        # Also sometimes it appears as if the thing in () is really a comment
        # e.g. The Angel in the Darkness (A novella of The Company)
        # almost a series but not quite.
        """
        logger.debug('_get_series_name_number series_title_components: {}'.
                     format(series_title_components))
        notes = self.generic_description
        # split on ; incase of multiple series
        series_titles = series_title_components.split(';', 1)
        logger.debug('got series title and maybe number: {}'.
                     format(series_titles[0]))
        # split off the book number from title: xyz, #12
        # group: 1 series name: xyz  group: 2 series number: 12
        title_number = re.search(r'^(.*?),? #(.+)$', series_titles[0])
        if not title_number:
            # What we thought was series title had no #number in it.
            notes += " Comment in title: " + series_titles[0]
            series_book_number = "0"
            series_title = series_titles[0]
            return series_title, series_book_number, notes

        series_title = title_number.group(1)
        logger.debug('got series title: {}'.
                     format(series_title))
        # Check series number, it is not always just a digit. E.g. 1a
        # or 1-3. If it is not just digits grab first group of digits, then
        # return notes so they can be added to the book notes
        series_numbers = re.search(r'^(\D*)(\d*)(\D*.*)$', title_number.group(2))
        series_book_number = series_numbers.group(2)
        if series_numbers.group(3):
            notes = " Series book number full: " + series_numbers.group(3)
        # if there was series info after a ; add it to the notes as well
        try:
            notes += " Extra series information: " + series_titles[1]
            logger.debug('adding extra series stuff to notes: {}'.
                         format(series_titles[1]))
        except IndexError:
            pass
        logger.debug("Returning title {0}, booknumber {1} and notes {2}".
                     format(series_title, series_book_number, notes))
        return series_title, series_book_number, notes

    def _find_series(self, series_title):
        """
        Finds a series object if it exists given the title

        Unfortunately series titles are not always consistent e.g.
           The Fitz and the Fool vs Fitz and the Fool
        also seen one where Trilogy was left off of the end.
           The Fitz and The Fool Trilogy

        Args: title
        Return: series object or None if not found
        """
        try:
            series = Series.objects.get(name=series_title)
        except Series.DoesNotExist:
            logger.debug('Checking for series again %s', series_title)
            # try again with The prepended and Trilogy appended.
            # remove first so can have them as an optional part of regexp.
            bare_title = re.sub(r'^The ', '', series_title)
            bare_title = re.sub(r' Trilogy', '', bare_title)
            bare_title = re.escape(bare_title)
            logger.debug('thing %s', r'^(The )?'+bare_title+r'( Trilogy)?$')
            seriess = Series.objects.filter(
                name__iregex=r'^(The )?'+bare_title+r'( Trilogy)?$')
            if len(seriess) == 1:
                series = seriess[0]
                logger.warning(
                    'Couldn\'t find "{0}" but found "{1}" '.
                    format(series_title, series.name))
            elif len(seriess) == 0:
                    series = None
            else:
                raise  # Multiple hits
        return series

    def _book_create_or_maybe_update(self, gr_book, title, author, series,
                                     series_number, notes, date_read,
                                     book_status):
        """
        Create or update the book.
        Find book just using title, author. A book is considered the same if
        the title and author are the same. Series information often changes.
        If series is not on incoming book assume it was updated manually and
        do not overwrite. If it has changed just warn.
        Not trying to keep goodreads and Mybooks in sync.
        If book is set as read in Mybooks then fields are not updated.
        If it was some other status then gr data is used to update.
        """
        created = False
        try:
            book = Book.objects.get(title=title, author=author)
            # If book has no series and series was on import add it
            if not book.series:
                if series:
                    book.series = series
                    book.series_book_number = series_number
            elif series and series != book.series:
                # Series that came in with import is different
                logger.warning("Book %s already has a series, %s import has a different one: %s",
                               book, book.series, series)
        except Book.DoesNotExist:
            book = Book.objects.create(
                title=title, author=author, series=series)
            created = True

        if created:
            logger.info(
                "\tCreated Book: {0} by {1}".format(title, gr_book['Author']))
        # If book has been created or unread update fields
        if created or not book.status == Book.READ:
            book.status = book_status
            book.rating = int(gr_book['My Rating'])
            book.notes = notes
            book.date_read = date_read
            book.save()
            if not created:
                logger.info("\tUpdated: {0} by {1}".format(title,
                                                           gr_book['Author']))
