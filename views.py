from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.generic import View, ListView, DetailView
from django.core.urlresolvers import reverse

from mybooks.models import Book, Series, Author
from mybooks.forms import AuthorForm

# List Views


class AuthorList(ListView):
    model = Author


class AuthorListRecent(View):
    def get(self, request):
        """Order authors by last read book"""
        pass


class SeriesList(ListView):
    model = Series


class BooksAlpha(View):
    def get(self, request):
        """Books ordered by Title"""
        book_list = Book.objects.order_by('title')
        template = 'mybooks/book_list.html'
        context = {'latest_book_list': book_list,
                   'list_title': 'All Alphabetical'}
        return render(request, template, context)


class BooksOrderRead(View):
    def get(self, request):
        """Books ordered by date read"""
        book_list = Book.objects.order_by('-date_read')
        template = 'mybooks/book_list.html'
        context = {'latest_book_list': book_list,
                   'list_title': 'All Ordered by Date Read'}
        return render(request, template, context)


class BookView(View):
    def get(self, request):
        book_list = Book.objects.order_by('-date_read')[:10]
        template = 'mybooks/book_list.html'
        context = {'latest_book_list': book_list,
                   'list_title': 'Lastest Read'}
        return render(request, template, context)


class BooksToRead(View):
    def get(self, request):
        # list books series started
        book_list_series_started = (
            Book.objects.exclude(status__in=(Book.READ, Book.BLOCK)).
            exclude(series=None).
            filter(series__status='S').
            order_by('series__name',
                     'series_book_number'))
        # list books series not started
        book_list_series = (
            Book.objects.exclude(status__in=(Book.READ, Book.BLOCK)).
            exclude(series=None).
            exclude(series__status=Series.STARTED).
            order_by('author__last', 'author__first', 'title'))
        # list other books
        book_list = (
            Book.objects.exclude(status=Book.READ).
            filter(series=None).
            order_by('author__last', 'author__first', 'title'))
        template = 'mybooks/book_list_to_read.html'
        context = {'list_title': "Books To Read Next",
                   'latest_book_list': book_list,
                   'latest_book_list_series_started': book_list_series_started,
                   'latest_book_list_series': book_list_series}
        return render(request, template, context)


# Detail views
class BookDetail(DetailView):
    model = Book


class SeriesDetail(DetailView):
    model = Series

    def get_context_data(self, **kwargs):
        context = super(SeriesDetail, self).get_context_data(**kwargs)
        context['books'] = Book.objects.filter(series=kwargs['object']).\
            order_by('series_book_number')
        return context


class AuthorDetail(DetailView):
    model = Author

    def get_context_data(self, **kwargs):
        context = super(AuthorDetail, self).get_context_data(**kwargs)
        context['books'] = Book.objects.filter(author=kwargs['object']).\
            order_by('title')
        return context


# TODO is to work out how to turn these indirect views
# to use the generic views
class BookSeries(SeriesDetail):
    def get_context_data(self, pk, **kwargs):
        '''Given book find series'''
        book = get_object_or_404(Book, pk)
        context = super(BookSeries, self).get_context_data(pk=book.series_id)
        return context


class BookAuthor(AuthorDetail):
    def get_context_data(self, pk, **kwargs):
        '''Given book find author'''
        book = get_object_or_404(Book, pk)
        context = super(Author, self).get_context_data(pk=book.author)
        return context


class AuthorUpdate(View):
    '''Given author id either display form or update '''
    def get(self, request, pk):
        author = get_object_or_404(Author, pk=pk)
        template = 'mybooks/author_update.html'
        context = {'author': author, 'pk': pk}
        return render(request, template, context)

    def post(self, request, pk):
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = get_object_or_404(Author, pk=pk)
            author.first = form.cleaned_data.get('first', None)
            author.last = form.cleaned_data.get('last', None)
            author.notes = form.cleaned_data.get('notes', None)
            if form.cleaned_data.get('rating', '') is None:
                author.rating = str(form.cleaned_data.get('rating',
                                    None))
            author.save()
            return HttpResponseRedirect(reverse('mybooks:author_detail',
                                                args=(author.id,)))
        else:
            return render(request, 'mybooks/author_update.html',
                          {'author': form, 'pk': pk})
