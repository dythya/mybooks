# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first', models.CharField(max_length=25)),
                ('last', models.CharField(max_length=25)),
                ('notes', models.TextField(null=True, blank=True)),
                ('rating', models.IntegerField(null=True, blank=True)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'Date added')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('series_book_number', models.IntegerField(null=True, blank=True)),
                ('status', models.CharField(default=b'M', max_length=2, choices=[(b'R', b'Read'), (b'S', b'Read Soon'), (b'M', b'Maybe Read'), (b'N', b'Read Next'), (b'M', b'Somewhere')])),
                ('description', models.TextField(null=True, blank=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('rating', models.IntegerField(null=True)),
                ('date_read', models.DateField(null=True, blank=True)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'Date added')),
                ('author', models.ForeignKey(to='mybooks.Author', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Series',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('next_book', models.CharField(max_length=100, null=True, blank=True)),
                ('status', models.CharField(default=b'L', max_length=2, choices=[(b'S', b'Started Reading'), (b'F', b'Series Complete'), (b'W', b'Waiting next book'), (b'U', b'Not even read book 1'), (b'L', b'Not added a status')])),
                ('rating', models.IntegerField(null=True, blank=True)),
                ('description', models.TextField()),
                ('notes', models.TextField(null=True, blank=True)),
                ('series_number', models.CharField(max_length=50, null=True, blank=True)),
                ('date_added', models.DateTimeField(auto_now_add=True, verbose_name=b'Date added')),
                ('next', models.ForeignKey(blank=True, to='mybooks.Series', null=True, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='book',
            name='series',
            field=models.ForeignKey(to='mybooks.Series', blank=True, null=True, db_index=False, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
