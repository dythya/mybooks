# Generated by Django 2.0.2 on 2018-02-19 09:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mybooks', '0015_series_date_updated'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='series',
            options={'ordering': ('primary_author', 'name'), 'verbose_name_plural': 'series'},
        ),
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='mybooks.Author'),
        ),
        migrations.AlterField(
            model_name='book',
            name='series',
            field=models.ForeignKey(blank=True, db_index=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to='mybooks.Series'),
        ),
        migrations.AlterField(
            model_name='series',
            name='next',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='mybooks.Series'),
        ),
        migrations.AlterField(
            model_name='series',
            name='primary_author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='mybooks.Author'),
        ),
    ]
